package src.bazaraki.model;

import com.google.common.collect.ForwardingSet;

import java.util.HashSet;
import java.util.Set;

public class SearchResult extends ForwardingSet<SearchResultData> {
    private Set<SearchResultData> delegate;

    public SearchResult(SearchResult result) {
        this.delegate = new HashSet<SearchResultData>(result.delegate);
    }

    public SearchResult() {
        this.delegate = new HashSet<SearchResultData>();
    }

    @Override
    protected Set<SearchResultData> delegate() {
        return delegate;
    }
}
