package src.bazaraki.model;

import java.time.LocalDateTime;

public class SearchResultData {
    private int id;
    private int photos;
    private LocalDateTime data;
    private Integer price;
    private String href;

    public SearchResultData withId(int id) {
        this.id = id;
        return this;
    }

    public SearchResultData withPhotos(int photos) {
        this.photos = photos;
        return this;
    }

    public SearchResultData withData(LocalDateTime data) {
        this.data = data;
        return this;
    }

    public SearchResultData withPrice(Integer price) {
        this.price = price;
        return this;
    }

    public SearchResultData withHref(String href) {
        this.href = href;
        return this;
    }

    public int getId() {
        return id;
    }

    public int getPhotos() {
        return photos;
    }

    public LocalDateTime getData() {
        return data;
    }

    public Integer getPrice() {
        return price;
    }

    public String getHref() {
        return href;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SearchResultData that = (SearchResultData) o;

        if (id != that.id) return false;
        if (photos != that.photos) return false;
        if (data != null ? !data.equals(that.data) : that.data != null) return false;
        if (price != null ? !price.equals(that.price) : that.price != null) return false;
        return href != null ? href.equals(that.href) : that.href == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + photos;
        result = 31 * result + (data != null ? data.hashCode() : 0);
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (href != null ? href.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SearchResultData{" +
                "id=" + id +
                ", photos=" + photos +
                ", data=" + data +
                ", price=" + price +
                ", href='" + href + '\'' +
                '}';
    }
}
