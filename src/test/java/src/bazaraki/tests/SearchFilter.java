package src.bazaraki.tests;

import org.testng.annotations.Test;
import src.bazaraki.model.SearchResult;
import src.bazaraki.model.SearchResultData;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class SearchFilter extends TestBase {
    @Test
    public void testSearchFilter() throws InterruptedException {
        app.goTo().searchPage();
        app.goTo().searchThat("2 bedrooms flat", "200", "1000");
        SearchResult allResults = app.searchResultHelper().getAllResults();
        SearchResult allResultsWithMore5Photos = app.searchResultHelper().getAllResultsWithMore5Photos(allResults);
        SearchResultData mostCheapestAndOldnessResult = app.searchResultHelper().getOldnessAndCheapestResult(allResultsWithMore5Photos);
        app.goTo().page(mostCheapestAndOldnessResult.getHref());
        app.resultPageHelper().addFavorite();
        app.loginPageHelper().acceptTerms();
        app.loginPageHelper().clickContinue();
        assertThat(app.loginPageHelper().getErrorMSG(), equalTo("The phone number is empty or not valid"));
    }
}
