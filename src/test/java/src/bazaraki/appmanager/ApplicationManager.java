package src.bazaraki.appmanager;

import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class ApplicationManager {
    private final Properties properties;
    WebDriver wd;
    TouchAction touchAction;

    private NavigationHelper navigationHelper;
    private SearchResultHelper searchResultHelper;
    private ResultPageHelper resultPageHelper;
    private LoginPageHelper loginPageHelper;
    private String browser;

    public ApplicationManager(String browser) {
        this.browser = browser;
        properties = new Properties();
    }

    public void init() throws IOException {
        String target = System.getProperty("target", "properties");
        properties.load(new FileReader(new File(String.format("src/test/resources/%s.properties", target))));

        if ("".equals(properties.getProperty("server"))) {
            if (browser.equals(BrowserType.CHROME)) {
                wd = new ChromeDriver();
            } else if (browser.equals(BrowserType.FIREFOX)) {
                wd = new FirefoxDriver();
            } else if (browser.equals(BrowserType.IE)) {
                wd = new InternetExplorerDriver();
            }
        } else {
            DesiredCapabilities capabilities = new DesiredCapabilities();
            capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, properties.getProperty("mobile.deviceName"));
            capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
            capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "9");
            capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "Chrome");
            wd = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
            touchAction = new TouchAction((PerformsTouchActions) wd);
        }

        wd.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        wd.get(properties.getProperty("web.baseUrl"));
        navigationHelper = new NavigationHelper(wd, properties);
        searchResultHelper = new SearchResultHelper(wd, properties);
        resultPageHelper = new ResultPageHelper(wd, properties);
        loginPageHelper = new LoginPageHelper(wd, properties);
    }

    public NavigationHelper goTo() {
        return navigationHelper;
    }

    public SearchResultHelper searchResultHelper() {
        return searchResultHelper;
    }

    public void stop() {
        wd.quit();
    }

    public ResultPageHelper resultPageHelper() {
        return resultPageHelper;
    }

    public LoginPageHelper loginPageHelper() {
        return loginPageHelper;
    }
}
