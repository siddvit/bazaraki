package src.bazaraki.appmanager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.Properties;

public class ResultPageHelper extends HelperBase {
    public ResultPageHelper(WebDriver wd, Properties properties) {
        super(wd);
        this.properties = properties;
    }

    private final Properties properties;

    public void addFavorite() {
        click(By.cssSelector(properties.getProperty("locators.resultPage.favorite")));
    }
}
