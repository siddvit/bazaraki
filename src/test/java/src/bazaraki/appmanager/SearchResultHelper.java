package src.bazaraki.appmanager;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import src.bazaraki.model.SearchResult;
import src.bazaraki.model.SearchResultData;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

public class SearchResultHelper extends HelperBase {
    public SearchResultHelper(WebDriver wd, Properties properties) {
        super(wd);
        this.properties = properties;
    }

    private final Properties properties;

    public SearchResult result = null;
    public int photos = 0;
    public int id = 0;
    public int price = 0;
    public String href = null;

    public DateTimeFormatter formatter = new DateTimeFormatterBuilder().appendPattern("dd.MM.yyyy HH:mm")
            .parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
            .parseDefaulting(ChronoField.MINUTE_OF_HOUR, 0)
            .toFormatter();

    public SearchResult getAllResults() {
        result = new SearchResult();
        List<WebElement> elements = wd.findElements(By.cssSelector(properties.getProperty("locators.resultSearch.container")));
        for (WebElement element : elements) {
            id = Integer.parseInt(element.findElement(By.cssSelector(properties.getProperty("locators.resultSearch.id"))).getAttribute("data-id"));
            try {
                photos = Integer.parseInt(element.findElement(By.cssSelector(properties.getProperty("locators.resultSearch.countPhotos"))).getText());
            } catch (NoSuchElementException e) {
                photos = 0;
            }
            LocalDateTime data = getData(element);
            price = Integer.parseInt(element.findElement(By.cssSelector(properties.getProperty("locators.resultSearch.price"))).getAttribute("content").split("\\.")[0]);
            href = element.findElement(By.cssSelector(properties.getProperty("locators.resultSearch.href"))).getAttribute("href");
            result.add(new SearchResultData().withId(id).withPhotos(photos).withPrice(price).withHref(href).withData(data));
        }
        return new SearchResult(result);
    }

    public SearchResult getAllResultsWithMore5Photos(SearchResult searchResult) {
        SearchResult result5Photo = new SearchResult();
        for (SearchResultData result : searchResult) {
            if (result.getPhotos() > 5) {
                result5Photo.add(result);
            }
        }
        return new SearchResult(result5Photo);
    }

    public SearchResultData getOldnessAndCheapestResult(SearchResult searchResult) {
        List<SearchResultData> resultData = searchResult.stream().sorted(((o1, o2) -> o1.getPrice() != o2.getPrice() ? o1.getPrice().compareTo(o2.getPrice()) : o1.getData().compareTo(o2.getData()))).collect(Collectors.toList());
        return resultData.get(0);
    }

    public LocalDateTime getData(WebElement element) {
        String data = element.findElement(By.cssSelector(properties.getProperty("locators.resultSearch.data"))).getText().split(",")[0];
        String time = data.split(" ")[1];
        String date = data.split(" ")[0];
        if (date.equals("Yesterday")) {
            date = yesterday();
        }
        String fullData = date + " " + time;
        return LocalDateTime.parse(fullData, formatter);
    }

    private String yesterday() {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return dateFormat.format(cal.getTime());
    }
}
