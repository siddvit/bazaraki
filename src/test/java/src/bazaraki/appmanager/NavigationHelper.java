package src.bazaraki.appmanager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.Properties;

public class NavigationHelper extends HelperBase {
    public NavigationHelper(WebDriver wd, Properties properties) {
        super(wd);
        this.properties = properties;
    }

    private final Properties properties;

    public void searchThat(String what_need, String min_price, String max_price) throws InterruptedException {
        // TODO: need optimization this method
        type(By.name(properties.getProperty("locators.searchPage.inputSearch")), what_need);
        click(By.name(properties.getProperty("locators.searchPage.inputSearch")));
        click(By.cssSelector("[class=\" dd-block  js-select-cities-regions\"]"));
        click(By.xpath("//li[@data-id='12']//span[1]"));
        click(By.xpath("//button[text()='Submit']"));
        select(By.xpath(properties.getProperty("locators.searchPage.minPrice")), By.cssSelector(String.format("[data-id='%s']", min_price)));
        click(By.name(properties.getProperty("locators.searchPage.inputSearch")));
        click(By.xpath("//section[contains(@class,'grey search-categories')]"));
        select(By.xpath(properties.getProperty("locators.searchPage.maxPrice")), By.xpath(String.format("(//li[@data-id='%s']//span)[2]", max_price)));
        click(By.xpath(properties.getProperty("locators.searchPage.searchBTN")));
        Thread.sleep(1000);
    }

    public void searchPage() {
        wd.get(properties.getProperty("web.baseUrl") + "search/");
    }

    public void page(String href) {
        wd.get(href);
    }
}
