package src.bazaraki.appmanager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.Properties;

public class LoginPageHelper extends HelperBase {
    public LoginPageHelper(WebDriver wd, Properties properties) {
        super(wd);
        this.properties = properties;
    }

    private final Properties properties;

    public void acceptTerms() {
        click(By.cssSelector(properties.getProperty("locators.loginPage.checkBoxAgreeTerms")));
    }

    public void clickContinue() {
        click(By.cssSelector(properties.getProperty("locators.loginPage.buttonContinue")));
    }

    public String getErrorMSG() {
        return get_msg(By.cssSelector(properties.getProperty("locators.loginPage.errorMSG")));
    }
}
